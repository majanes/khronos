This directory contains headers for all relevant Khronos APIs, based from:

* http://www.khronos.org/

* http://www.opengl.org/registry/

* http://oss.sgi.com/projects/ogl-sample/


To update simply run:

    make

This project was taken from Apitrace's thirdparty projects, so it
could be included in a meson-compatible subproject structure:

```https://gitlab.freedesktop.org/majanes/apitrace/-/tree/master/thirdparty/khronos```

